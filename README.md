# Introduction 

Technology stack:
to acheive the required spescs, I created 4 images used in docker-compose.yaml
- production_nginx: a proxy server to access both airports and countries APIS, configuratin can be found on nginx.conf
- production_jenkins:for Automation of the deployment.
- airports: the airports API container.
- countries: the countries API container.

# Steps to tun the sample 
- $ git clone ....
- $ cd airports && docker build -t airports .
- $  cd countries && docker build -t countries .
- $  docker-compose up 
- navigate to : http://localhost:8000/airports | http://localhost:8000/countries
- For publishing version 1.1.0 of the airport service I used jenkins
 	* navigate to http://localhost:8080, login with the password appeared in 		docker console.
	* install required plugins ,then Publish Over SSH
- Upgrade the service on runtime http://localhost:8080/job/UpgradeAirports/build?token=abc

### Publish Over SSH settings 
* jenkins configuration 
    * Publish over SSH : add new server jenkins@airports:jenkins000
* Add new  jenkins job 
    * Trigger builds remotely Authentication Token :abc , this will be called like http://localhost:8080/job/UpgradeAirports/build?token=abc  
    * select Send build artifacts over SSH 
        Transfer set 1 
        Remote directory /home/jenkins/
        ```
        #!/bin/bash
        FILE=/home/jenkins/airports-assembly-1.0.1.pid
        if [ -f "$FILE" ];
        then
        read pid < "$FILE"
        kill "$pid"
        rm "$FILE"
        fi
        ```
        
        Transfer set 2
        Source files /var/jenkins_home/deploy/airports/airports-assembly-1.1.0.jar
        Remove prefix /deploy/airports
        Remote directory /home/jenkins/
        Exec command
        ```
        #!/bin/bash
        nohup java -jar /home/jenkins/airports-assembly-1.1.0.jar &
        echo $! >> /home/jenkins/airports-assembly-1.1.0.pid
        sleep 1
        ```
